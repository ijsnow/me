import { NextPage } from "next";
import { useRouter } from "next/router";

const Index: NextPage = () => {
  const { query } = useRouter();

  return (
    <div className="container">
      <h1>
        <span>Isaac Snow</span>
      </h1>
      <h3>I use technology to solve problems.</h3>
      <h3>Available for full time work as well as consulting.</h3>
      <ul>
        <li>
          Contact me:{" "}
          <a href="mailto:isaacjsnow@gmail.com">isaacjsnow@gmail.com</a>
        </li>
        <li>
          <a href="https://gitlab.com/ijsnow">GitLab/ijsnow</a>
        </li>
        <li>
          <a href="https://github.com/ijsnow">GitHub/ijsnow</a>
        </li>
        <li>
          <a href="/static/pdf/resume.pdf">Resume</a>
        </li>
      </ul>
      {query.showOs && (
        <>
          <h3>Open Source</h3>
          <ul>
            <li>
              Worked at{" "}
              <a href="https://github.com/sourcegraph/sourcegraph">
                Sourcegraph.
              </a>{" "}
              I spent time working on the web application, search team and on
              the browser extension leading the effort to integrate with more
              code hosts.
            </li>
            <li>
              <a href="https://github.com/ijsnow/studiojs">studio.js</a>
              <p>
                A collection of tools to create your own recording studio in
                browser.
              </p>
              <p>
                <a href="https://www.npmjs.com/package/recorder-js">
                  Recorder-js
                </a>
                &nbsp;is an easy to use audio recorder.
              </p>
              <p>
                <a href="https://www.npmjs.com/package/react-wave-stream">
                  react-wave-stream
                </a>
                &nbsp;visualizes frequency data from audio stream.
              </p>
            </li>
            <li>
              <a href="https://github.com/ijsnow/react-simple-accordion">
                react-simple-accordion
              </a>
              <p>A simple accordion built with React.</p>
            </li>
            <li>
              <a href="https://github.com/ijsnow/gittp">gittp</a>
              <p>Easily serve git repos over http.</p>
            </li>
            <li>
              <a href="https://github.com/storybooks/storybook-ui/pull/70">
                Minor contribution
              </a>{" "}
              to <a href="https://storybooks.js.org/">react storybooks</a>
            </li>
          </ul>
        </>
      )}
      <style jsx>{`
        .container {
          max-width: 500px;
          margin: 0 auto;
        }

        h1 {
          border-bottom: 3px solid #0079ff;
        }

        h3 {
          font-weight: normal;
        }

        a:hover {
          color: #0079ff;
        }

        a {
          color: #000;
          text-decoration: none;
          border-bottom: 1px solid #0079ff;
        }

        a:hover {
          color: #0079ff;
        }
      `}</style>
    </div>
  );
};

export default Index;
