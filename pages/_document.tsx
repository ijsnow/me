import Document, {
  Html,
  Main,
  NextScript,
  DocumentContext
} from "next/document";
import flush from "styled-jsx/server";

class CustomDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const initialProps = await Document.getInitialProps(ctx);
    const styles = flush();
    return { styles, ...initialProps };
  }

  render() {
    return (
      <Html>
        <head>
          <link
            href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap"
            rel="stylesheet"
          />
          <link
            href="https://fonts.googleapis.com/css?family=Roboto&display=swap"
            rel="stylesheet"
          />
          {this.props.styles}
        </head>
        <body>
          <Main />
          <NextScript />
          <style global jsx>{`
            body {
              margin: 0;
              font-family: "Open Sans", sans-serif;
            }

            h1 {
              font-family: "Roboto", sans-serif;
            }
          `}</style>
        </body>
      </Html>
    );
  }
}

export default CustomDocument;
